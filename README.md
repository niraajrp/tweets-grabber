# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ##

* Python Script for Facebook Ads for Coles Insurance Reports



## How do I get set up? ##

* S3 Folder : annalects3. Rest of the folders are created automatially based on dates

### Environment Variables to be defines: ###
* startDATE (Optional):  (From when to fetch the data) (If you dont supply it calculates as yesterday or based on past runs)
* endDATE (Optional) :   (Till when to fetch the data) (If you dont supply it calculates as yesterday)
* AdAccountId: Mandatory

### Environment Passwords to be defined: ###
* CLIENT_ID : It is the app_id of the Facebook App
* CLIENT_SECRET : It is the app_secret for the Facebook app being used
* ACCESS_TOKEN : Supply the previously obtained access_token

### Redshift Tables: ###
* Digital.cfs_facebook_insurance_master
* Digital.cfs_facebook_insurance_stg

	* Coulmns in both the tables are same.Details are as follows

 	    * reporting_starts                **date,**
        * reporting_ends                  **date,**
        * ad_name                         **varchar(200),**
        * age_range                       **varchar(64),**
        * gender                          **varchar(64),**
        * reach                           **integer,**
        * amount_spent_aud                **varchar(200),**
        * frequency                       **varchar(200),**
        * impressions                     **integer,**
        * post_shares                     **integer,**
        * post_reaction                   **integer,**
        * post_engagement                 **integer,**
        * post_comment                    **integer,**
        * cpm_cost_per_1000_impressions   **varchar(200),**
        * cpc_cost_per_link_click_aud     **varchar(200),**
        * ctr_link_click_through_rate     **varchar(200),**
        * gi_home_approved                **integer,**
        * gi_car_declined                 **integer,**
        * gi_car_referred                 **integer,**
        * gi_home_declined                **integer,**
        * gi_car_approved                 **integer,**
        * gi_home_referred                **integer,**
        * video_views_3_sec               **integer,**
        * video_views_30_sec              **integer,**
        * cost_per_3sec_video_view_aud    **varchar(200),**
        * video_views_10_sec              **integer,**
        * cost_per_10sec_video_views_aud  **varchar(200),**
        * video_watches_25_percent        **integer,**
        * video_watches_50_percent        **integer,**
        * video_watches_75_percent        **integer,**
        * video_watches_95_percent        **integer,**
        * video_watches_100_percent       **integer,**
        * link_clicks                     **integer,**
        * website_purchases               **integer**



